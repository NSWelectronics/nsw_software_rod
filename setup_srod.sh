#!/bin bash

#######################################################
#
# set the environment for compiling & running software
# rod software package, phase1_readout_prototyping
#
# puts ROx in your path
#
# daniel.joseph.antrim@cern.ch
# April 2017
#
#######################################################

#if [[ ! -d "felix_software" ]]
#    then
#        echo "ERROR cannot find felix_software/ directory"
#        return 1
#fi
#
#if [[ ! -d "phase1_readout_prototyping" ]]
#    then
#        echo "ERROR cannot find phase1_readout_prototyping/ directory"
#        return 1
#fi
#
#echo "Setting SRODDIR=${PWD}"
#export SRODDIR=${PWD}
#
#echo "source ${SRODDIR}/felix_software/cmake_tdaq/bin/setup.sh x86_64-slc6-gcc49-opt"
#
#rox_dir=${SRODDIR}/phase1_readout_prototyping/build/ROx
#export PATH=${rox_dir}:$PATH

function main {

    if [[ ! -d "felix_software" ]]
        then
            echo "ERROR cannot find felix_software/ directory in present working directory"
            return 1
    fi

    if [[ ! -d "phase1_readout_prototyping" ]]
        then
            echo "ERROR cannot find phase1_readout_prototyping/ directory in present working directory"
            return 1
    fi

    echo ""
    echo "Setting environment SRODDIR to ${PWD}"
    echo ""
    export SRODDIR=${PWD}

    arch=x86_64-slc6-gcc49-opt
    source ${SRODDIR}/felix_software/cmake_tdaq/bin/setup.sh ${arch}

    rox_dir=${SRODDIR}/phase1_readout_prototyping/build/ROx
    export PATH=${rox_dir}:$PATH

}

main $*
