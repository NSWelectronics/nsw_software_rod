# nsw_software_rod

# Contents

* [Introduction](#introduction)
* [Steps](#steps-you-should-take)
* [Qs](#questions)


## Introduction

This package sets up the working directory structure required to run the core [FELIX
software](https://gitlab.cern.ch/atlas-tdaq-felix/software) and the prototype
software ROD software [phase1_readout_prototyping](https://gitlab.cern.ch/crone/phase1_readout_prototyping)
in the context of the NSW readout.

## Steps you should take

Once you have obtained this repository, do:

```
cd nsw_software_rod/
./setup_area - -
./install_area
```

The script ```setup_area``` checks out the two software bases' repositories. It takes two
positional arguments, the first of which is the tag of the FELIX software that you
wish to checkout (default is ```felix-03-04-02```) and the second is the tag/branch
of phase1_readout_prototyping to checkout (default is ```nswdev```). Note that the
```nswdev``` branch of phase1_readout_prototyping is the NSW "master" branch
in this repository and has modified installation (e.g. CMakeLists.txt) as compared
to the ```master``` branch of phase1_readout_prototyping. If you wish to setup only
the default tags, provide a ```-``` for both the first and second arguments of ```setup_area```.

The script ```install_area``` assumes that you have run ```setup_area``` succesfully
already. It runs the installation steps required for the FELIX software and for
phase1_readout_prototyping. This script takes a single argument, which is a
flag either ```--rod-only``` or ```--felix-only``` requesting to perform compilation
of either phase1_readout_prototyping or the felix software, respectively. If you
do not provide any flags it will compile both packages.

For either scripts, provide ```--help``` to print the usage. **You should execute
these scripts**.

There is also the script ```setup_srod.sh``` which you can source. This script
should be used when you wish to run either the FELIX software or the phase1_readout_software
software. It adds an environment variable ```SRODDIR``` which is used for
compiling phase1_readout_software but also sets the environment used during
installation to pick the correct libraries at runtime.

## Questions
For any help or feedback please contact us: [Daniel Joseph Antrim, Johanna Gramling](mailto:daniel.joseph.antrim@cern.ch,jgramlin@cern.ch)